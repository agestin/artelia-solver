import java.util.ArrayList;

public class Equipement {
	
	String nom;
	int orientation;
	int x;
	int y;
	int longueur;
	int largeur;
	int hauteur;
	int zone_thermique;
	ArrayList<Integer> cote_maintenance = new ArrayList<Integer>();
	int zone_maintenance;
	boolean mur; //true si coll� au mur
	boolean sol; //true si coll� au sol
	
	
	public Equipement() {
	}
	
	/**
	 * Pour les dimensions du local
	 * @param largeur
	 * @param longueur 
	 * @param hauteur
	 */
	public Equipement(int largeur, int longueur, int hauteur) {
		super();
		this.largeur = largeur;
		this.longueur = longueur;
		this.hauteur = hauteur;
	}
	
	/**
	 * Pour les �quipements de type porte
	 * @param x
	 * @param y
	 * @param largeur
	 * @param hauteur
	 */
	public Equipement(int x, int y, int largeur, int hauteur) {
		super();
		this.x = x;
		this.y = y;
		this.largeur = largeur;
		this.hauteur = hauteur;
	}

	/**
	 * Pour les equipements �nerg�tiques
	 * @param longueur
	 * @param largeur
	 * @param hauteur
	 * @param zone_thermique
	 * @param zone_maintenance
	 */
	public Equipement(int largeur, int longueur, int hauteur, int zone_thermique, int zone_maintenance) {
		super();
		this.largeur = largeur;
		this.longueur = longueur;
		this.hauteur = hauteur;
		this.zone_thermique = zone_thermique;
		this.zone_maintenance = zone_maintenance;
	}

	public Equipement(String nom, int longueur, int largeur, int hauteur, int zone_thermique,
			ArrayList<Integer> cote_maintenance, int zone_maintenance, boolean mur) {
		super();
		this.nom = nom;
		this.longueur = longueur;
		this.largeur = largeur;
		this.hauteur = hauteur;
		this.zone_thermique = zone_thermique;
		this.cote_maintenance = cote_maintenance;
		this.zone_maintenance = zone_maintenance;
		this.mur = mur;
	}
	
	public String getNom() {
		return nom;
	}
	
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public int getX() {
		return x;
	}
	
	public void setX(int x) {
		this.x = x;
			}
	
	public int getY() {
		return y;
	}
	
	public void setY(int y) {
		this.y = y;
	}
	
	public int getOrientation() {
		return orientation;
	}

	public void setOrientation(int orientation) {
		this.orientation = orientation;
	}

	public int getLongueur() {
		return longueur;
	}

	public void setLongueur(int longueur) {
		this.longueur = longueur;
	}

	public int getLargeur() {
		return largeur;
	}

	public void setLargeur(int largeur) {
		this.largeur = largeur;
	}

	public int getHauteur() {
		return hauteur;
	}

	public void setHauteur(int hauteur) {
		this.hauteur = hauteur;
	}

	public int getZone_thermique() {
		return zone_thermique;
	}

	public void setZone_thermique(int zone_thermique) {
		this.zone_thermique = zone_thermique;
	}

	public ArrayList<Integer> getCote_maintenance(){
		return cote_maintenance;
	}

	public void setCote_maintenance(ArrayList<Integer> cote_maintenance) {
		this.cote_maintenance = cote_maintenance;
	}
	
	public int getZone_maintenance() {
		return zone_maintenance;
	}

	public void setZone_maintenance(int zone_maintenance) {
		this.zone_maintenance = zone_maintenance;
	}

	public boolean isMur() {
		return mur;
	}

	public void setMur(boolean mur) {
		this.mur = mur;
	}

	public boolean isSol() {
		return sol;
	}

	public void setSol(boolean sol) {
		this.sol = sol;
	}

	@Override
	public String toString() {
		return "Equipement [nom=" + nom + ", longueur=" + longueur + ", largeur=" + largeur + ", hauteur=" + hauteur
				+ ", zone_thermique=" + zone_thermique + ", cote_maintenance=" + cote_maintenance + ", zone_maintenance=" + zone_maintenance
				+ ", mur=" + mur + "]";
	}
}
