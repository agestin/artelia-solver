import java.awt.Color;
import java.awt.Graphics;
import java.util.List;
import java.util.ArrayList;

import org.chocosolver.solver.Model;
import org.chocosolver.solver.Solution;
import org.chocosolver.solver.search.limits.SolutionCounter;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.solver.variables.RealVar;
import org.chocosolver.solver.variables.SetVar;

import com.opencsv.CSVWriter;

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.*;

import javax.swing.JFrame;

public class Modele2D extends JPanel{
	
	public static void modele2D(Solveur_2D t){
		JFrame frame = new JFrame("Représentation 2D");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		if (t.map.get("solCount")!=0){

		//ajout label porte
		Label lporte;
		lporte = new Label("Porte");
		lporte.setBounds(t.map.get("x_porte")+5, t.map.get("y_porte")+5,40,30);
		frame.add(lporte);
		
		//ajout label equipement
		Label[] l = new Label[t.nb_equipements];
		for (int i=1; i<=t.nb_equipements; i++) {
			l[i-1] = new Label("Equipement " +i);
			l[i-1].setBounds(t.map.get("x_"+i)+5, t.map.get("y_"+i)+5,85, 30);
			frame.add(l[i-1]);
		}
		
		frame.add(t);
		frame.setSize(1000,1000);
		frame.setVisible(true);
		}
	}
	
	public static void main (String[] args) throws IOException   {
		Solveur_2D t = new Solveur_2D();
		
		t.solveur();
		
		modele2D(t);
		if (t.map.get("solCount")!=0){
			modele2D(t);
			t.writeDataLineByLine("./src/csvOutput" );
		}
	}

}