public class Local {
	
	String nom;
	int longueur;
	int largeur;
	int hauteur;
	
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public String getNom() {
		return nom;
	}
	
	public int getLongueur() {
		return longueur;
	}

	public void setLongueur(int longueur) {
		this.longueur = longueur;
	}

	public int getLargeur() {
		return largeur;
	}

	public void setLargeur(int largeur) {
		this.largeur = largeur;
	}

	public int getHauteur() {
		return hauteur;
	}

	public void setHauteur(int hauteur) {
		this.hauteur = hauteur;
	}
	
	@Override
	public String toString() {
		return "Local [nom=" + nom + ", longueur=" + longueur + ", largeur=" + largeur + ", hauteur=" + hauteur + "]";
	}	

}
