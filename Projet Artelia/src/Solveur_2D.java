import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

import org.apache.poi.util.SystemOutLogger;
import org.chocosolver.solver.Model;
import org.chocosolver.solver.Solution;
import org.chocosolver.solver.constraints.Constraint;
import org.chocosolver.solver.search.limits.SolutionCounter;
import org.chocosolver.solver.variables.BoolVar;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.solver.variables.RealVar;
import org.chocosolver.solver.variables.SetVar;

import com.opencsv.CSVWriter;

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.*;

import javax.swing.JFrame;

public class Solveur_2D extends JPanel {

	/**
	 * Reference : K03-05
	 */



	public  Map<String,Integer> map = new HashMap<String,Integer>();
	
	// Parseur
	public Parseur parseur = new Parseur("data.xls");
	int nb_equipements = 0;
	int nb_zones_maintenance = 0;
	int nb_poteaux = 0;
	
		public  void solveur() throws IOException {


			
			/**
			 * Data
			 */
		
			//Local
			Local local = parseur.getLocal();

			int l_local = local.getLargeur();
			int L_local = local.getLongueur();
			int h_local = local.getHauteur();
			
			map.put("L_local", L_local);
			map.put("l_local", l_local);
			map.put("h_local", h_local);
					
			// Porte
			Porte porte = parseur.getPorte();

			int x_porte = porte.getX();
			int y_porte = porte.getY();
			int l_porte = porte.getLargeur();
			int h_porte = porte.getHauteur();
			
			map.put("x_porte", x_porte);
			map.put("y_porte", y_porte);
			map.put("l_porte", l_porte);
			
			// Poteaux
			List<Poteau> poteaux = parseur.getPoteaux();
			
			int n_poteaux = poteaux.size();
			nb_poteaux = n_poteaux;
			
			ArrayList<Integer> x_p = new ArrayList<Integer>();
			ArrayList<Integer> y_p = new ArrayList<Integer>();
			ArrayList<Integer> L_p = new ArrayList<Integer>();
			ArrayList<Integer> l_p = new ArrayList<Integer>();
			ArrayList<Integer> h_p = new ArrayList<Integer>();
			
			for (int i=0; i<n_poteaux; i++) {
				x_p.add(((Poteau) poteaux.get(i)).getX());
				y_p.add(((Poteau) poteaux.get(i)).getY());
				L_p.add(((Poteau) poteaux.get(i)).getLongueur());
				l_p.add(((Poteau) poteaux.get(i)).getLargeur());
				h_p.add(((Poteau) poteaux.get(i)).getHauteur());
			}			
			
			// �quipements
			List<Equipement> equipements = parseur.getEquipements();


			System.out.println(equipements);
			int n_equipements = equipements.size(); //Nombre d'�quipements
			nb_equipements = n_equipements;
			
			ArrayList<Integer> l = new ArrayList<Integer>();
			ArrayList<Integer> L = new ArrayList<Integer>();
			ArrayList<Integer> h = new ArrayList<Integer>();
			ArrayList<Integer> z_t = new ArrayList<Integer>();
			ArrayList<Integer> z_m = new ArrayList<Integer>();
			ArrayList<ArrayList<Integer>> c_m = new ArrayList<ArrayList<Integer>>();
			
			Set num_col_au_mur = new HashSet<Integer>();
			ArrayList<Boolean> mur = new ArrayList<>();

			
			for (int i=0; i<n_equipements; i++) {
				l.add(((Equipement) equipements.get(i)).getLargeur());
				L.add(((Equipement) equipements.get(i)).getLongueur());
				h.add(((Equipement) equipements.get(i)).getHauteur());
				z_t.add(((Equipement) equipements.get(i)).getZone_thermique());
				z_m.add(((Equipement) equipements.get(i)).getZone_maintenance());
				nb_zones_maintenance += ((Equipement) equipements.get(i)).getCote_maintenance().size();
				c_m.add(((Equipement) equipements.get(i)).getCote_maintenance());
				mur.add(((Equipement) equipements.get(i)).isMur());
				if (((Equipement) equipements.get(i)).isMur()) {
					num_col_au_mur.add(i);
				}
			}
			
				/**
				 * Zones thermiques & maintenances
				 */
			
			int[] l_t = new int[n_equipements];
			int[] L_t = new int[n_equipements];
			int[] l_m = new int[n_equipements];
			int[] L_m = new int[n_equipements];
			for (int i = 0; i < n_equipements; i++) {
				l_t[i] = l.get(i) + 2*z_t.get(i);
				L_t[i] = L.get(i) + 2*z_t.get(i);
			}	
		
			/**
			 * Model
			 */
			Model model = new Model("Artelia");
			
			IntVar l_local_Var = model.intVar("l_local", l_local); // largeur �quipement local
			IntVar L_local_Var = model.intVar("L_local", L_local); // longueur �quipement local
			IntVar h_local_Var = model.intVar("h_local", h_local); // hauteur quipement local

			
			IntVar[] x_Var = new IntVar[n_equipements + n_poteaux + 1]; // liste des abscises des �quipements + porte
			IntVar[] y_Var = new IntVar[n_equipements + n_poteaux + 1]; // liste des ordonn�es des �quipements + porte
			IntVar[] z_Var = new IntVar[n_equipements + n_poteaux + 1]; // liste des coordonn�es z des �quipements + porte
			IntVar[] l_Var = new IntVar[n_equipements + n_poteaux + 1]; // liste des largeurs des �quipements + porte
			IntVar[] L_Var = new IntVar[n_equipements + n_poteaux + 1]; // liste des longueurs des �quipements + porte
			IntVar[] h_Var = new IntVar[n_equipements + n_poteaux + 1]; // liste des hauteurs des �quipements + porte

			
			IntVar[] z_t_Var = new IntVar[n_equipements]; // liste des dimensions des zones thermiques
			IntVar[] x_t_Var = new IntVar[n_equipements]; // liste des abscisses des zones thermiques
			IntVar[] y_t_Var = new IntVar[n_equipements]; // liste des ordonn�es des zones thermiques
			IntVar[] l_t_Var = new IntVar[n_equipements]; // liste des largeurs des zones thermiques
			IntVar[] L_t_Var = new IntVar[n_equipements]; // liste des longueurs des zones thermiques
			
			ArrayList<ArrayList<IntVar>> x_m_Var = new ArrayList<ArrayList<IntVar>>();
			ArrayList<ArrayList<IntVar>> y_m_Var = new ArrayList<ArrayList<IntVar>>();
			ArrayList<ArrayList<IntVar>> l_m_Var = new ArrayList<ArrayList<IntVar>>();
			ArrayList<ArrayList<IntVar>> L_m_Var = new ArrayList<ArrayList<IntVar>>();
			ArrayList<IntVar> z_m_Var = new ArrayList<IntVar>();
			ArrayList<ArrayList<BoolVar>> zone1 = new ArrayList<ArrayList<BoolVar>>();
			ArrayList<ArrayList<BoolVar>> zone2 = new ArrayList<ArrayList<BoolVar>>();
			ArrayList<ArrayList<BoolVar>> zone3 = new ArrayList<ArrayList<BoolVar>>();
			ArrayList<ArrayList<BoolVar>> zone4 = new ArrayList<ArrayList<BoolVar>>();
			ArrayList<ArrayList<BoolVar>> casA = new ArrayList<ArrayList<BoolVar>>();
			ArrayList<ArrayList<BoolVar>> casB = new ArrayList<ArrayList<BoolVar>>();
			ArrayList<ArrayList<BoolVar>> casC = new ArrayList<ArrayList<BoolVar>>();
			ArrayList<ArrayList<BoolVar>> casD = new ArrayList<ArrayList<BoolVar>>();
			
			IntVar[] sens = new IntVar[n_equipements];
			BoolVar[] sens1 = new BoolVar[n_equipements];
			BoolVar[] sens2 = new BoolVar[n_equipements];
			BoolVar[] sens3 = new BoolVar[n_equipements];
			BoolVar[] sens4 = new BoolVar[n_equipements];
			
			Constraint[] c_horizontal = new Constraint[n_equipements];
			Constraint[] c_vertical = new Constraint[n_equipements];
			
			/**
			 * Variables
			 */
			
				/**
				 * Variables �quipements 
				 */
			
			for (int i = 0; i < (n_equipements + n_poteaux + 1); i++) {
				if (i < n_equipements) {
					x_Var[i] = model.intVar("x_"+(i+1), 0, L_local, true); // position x 
					y_Var[i] = model.intVar("y_"+(i+1), 0, l_local, true); // position y 
					z_Var[i] = model.intVar("z_"+(i+1), 0, h_local, true); // position y 
					l_Var[i] = model.intVar("l_"+(i+1), Math.min(l.get(i), L.get(i)), Math.max(l.get(i), L.get(i))); // largeur
					L_Var[i] = model.intVar("L_"+(i+1), Math.min(l.get(i), L.get(i)), Math.max(l.get(i), L.get(i))); // longueur 
					h_Var[i] = model.intVar("h_"+(i+1), h.get(i)); // hauteur
 
				}  
				if ((i >= n_equipements) && i < (n_equipements + n_poteaux)) {
					x_Var[i] = model.intVar("x_poteau_"+i, x_p.get(i-n_equipements)); // position x 
					y_Var[i] = model.intVar("y_poteau_"+i, y_p.get(i-n_equipements)); // position y 
					z_Var[i] = model.intVar("z_poteau_"+i, 0); // position z d'un poteau fix�e � 0
					l_Var[i] = model.intVar("l_poteau_"+i, L_p.get(i-n_equipements)); // largeur
					L_Var[i] = model.intVar("L_poteau_"+i, l_p.get(i-n_equipements)); // longueur 
					h_Var[i] = model.intVar("h_poteau_"+i, h_p.get(i-n_equipements)); // hauteur

				} 
				if (i>= n_equipements + n_poteaux) {
					x_Var[i] = model.intVar("x_porte", x_porte);
					y_Var[i] = model.intVar("y_porte", y_porte);
					z_Var[i] = model.intVar("z_porte", 0); //position z de la porte fix�e � 0
					l_Var[i] = model.intVar("l_porte", l_porte);
					L_Var[i] = model.intVar("L_porte", l_porte);
					h_Var[i] = model.intVar("h_porte", h_porte);

				}

			}
			
				/**
				 * Variables zones thermiques
				 */
			
			for (int i = 0; i < n_equipements; i++) {
				z_t_Var[i] = model.intVar("z_t_"+(i+1), z_t.get(i));
				x_t_Var[i] = model.intVar("x_t_"+(i+1), 0, L_local, true); 
				y_t_Var[i] = model.intVar("y_t_"+(i+1), 0, l_local, true); 
				l_t_Var[i] = model.intVar("l_t_"+(i+1), Math.min(l_t[i], L_t[i]), Math.max(l_t[i], L_t[i]));
				L_t_Var[i] = model.intVar("L_t_"+(i+1), Math.min(l_t[i], L_t[i]), Math.max(l_t[i], L_t[i]));
			}
			
				/**
				 * Variables zones maintenances
			 	*/
		
			for (int i = 0; i < n_equipements; i++) {
				z_m_Var.add(model.intVar("z_m_"+(i+1), z_m.get(i)));
				ArrayList<IntVar> x_m_Var_i = new ArrayList<IntVar>(); 
				ArrayList<IntVar> y_m_Var_i = new ArrayList<IntVar>(); 
				ArrayList<IntVar> l_m_Var_i = new ArrayList<IntVar>(); 
				ArrayList<IntVar> L_m_Var_i = new ArrayList<IntVar>(); 
				for(int j=0; j<c_m.get(i).size(); j++) {
					x_m_Var_i.add(model.intVar("x_m_"+(i+1)+"_"+(j+1), 0, L_local, true));
					y_m_Var_i.add(model.intVar("y_m_"+(i+1)+"_"+(j+1), 0, l_local, true));
					l_m_Var_i.add(model.intVar("l_m_"+(i+1)+"_"+(j+1), Math.min(l.get(i), Math.min(L.get(i), z_m.get(i))), Math.max(l.get(i), Math.max(L.get(i), z_m.get(i)))));
					L_m_Var_i.add(model.intVar("L_m_"+(i+1)+"_"+(j+1), Math.min(l.get(i), Math.min(L.get(i), z_m.get(i))), Math.max(l.get(i), Math.max(L.get(i), z_m.get(i)))));
				}
				x_m_Var.add(x_m_Var_i);
				y_m_Var.add(y_m_Var_i);
				l_m_Var.add(l_m_Var_i);
				L_m_Var.add(L_m_Var_i);
			}
			
				/**
				 * Mur
				 */
			
			Iterator it = num_col_au_mur.iterator();
			while (it.hasNext()) {
				int index = (int)it.next();
				BoolVar b1 = model.arithm(x_Var[index], "=", L_local).reify();
				BoolVar b2 = model.arithm(x_Var[index], "=", 0).reify();
				BoolVar b3 = model.arithm(y_Var[index], "=", l_local).reify();
				BoolVar b4 = model.arithm(y_Var[index], "=", 0).reify();
				model.or(b1,b2,b3,b4).post();
			}
			
				/**
				 * Sens initialisation
				 */
		
			for (int i = 0; i < (n_equipements); i++) {
				sens[i] = model.intVar("sens_"+i, 0, 3);
				sens1[i] = model.arithm(sens[i], "=", 0).reify();
				sens2[i] = model.arithm(sens[i], "=", 1).reify();
				sens3[i] = model.arithm(sens[i], "=", 2).reify();
				sens4[i] = model.arithm(sens[i], "=", 3).reify();
				
				c_horizontal[i] = model.arithm(sens1[i], "+", sens3[i], "=", 1);
				c_vertical[i] = model.arithm(sens2[i], "+", sens4[i], "=", 1);
				
				ArrayList<BoolVar> zone1_i = new ArrayList<BoolVar>();
				ArrayList<BoolVar> zone2_i = new ArrayList<BoolVar>();
				ArrayList<BoolVar> zone3_i = new ArrayList<BoolVar>();
				ArrayList<BoolVar> zone4_i = new ArrayList<BoolVar>();
				for(int j=0; j<c_m.get(i).size(); j++) {
					zone1_i.add(model.arithm(model.intVar(c_m.get(i).get(j)), "=", 1).reify());
					zone2_i.add(model.arithm(model.intVar(c_m.get(i).get(j)), "=", 2).reify());
					zone3_i.add(model.arithm(model.intVar(c_m.get(i).get(j)), "=", 3).reify());
					zone4_i.add(model.arithm(model.intVar(c_m.get(i).get(j)), "=", 4).reify());
				}
				zone1.add(zone1_i);
				zone2.add(zone2_i);
				zone3.add(zone3_i);
				zone4.add(zone4_i);
				
				ArrayList<BoolVar> casA_i = new ArrayList<BoolVar>();
				ArrayList<BoolVar> casA_i_1 = new ArrayList<BoolVar>();
				ArrayList<BoolVar> casA_i_2 = new ArrayList<BoolVar>();
				ArrayList<BoolVar> casA_i_3 = new ArrayList<BoolVar>();
				ArrayList<BoolVar> casA_i_4 = new ArrayList<BoolVar>();
				ArrayList<BoolVar> casA_i_12 = new ArrayList<BoolVar>();
				ArrayList<BoolVar> casA_i_34 = new ArrayList<BoolVar>();
				ArrayList<BoolVar> casB_i = new ArrayList<BoolVar>();
				ArrayList<BoolVar> casB_i_1 = new ArrayList<BoolVar>();
				ArrayList<BoolVar> casB_i_2 = new ArrayList<BoolVar>();
				ArrayList<BoolVar> casB_i_3 = new ArrayList<BoolVar>();
				ArrayList<BoolVar> casB_i_4 = new ArrayList<BoolVar>();
				ArrayList<BoolVar> casB_i_12 = new ArrayList<BoolVar>();
				ArrayList<BoolVar> casB_i_34 = new ArrayList<BoolVar>();
				ArrayList<BoolVar> casC_i = new ArrayList<BoolVar>();
				ArrayList<BoolVar> casC_i_1 = new ArrayList<BoolVar>();
				ArrayList<BoolVar> casC_i_2 = new ArrayList<BoolVar>();
				ArrayList<BoolVar> casC_i_3 = new ArrayList<BoolVar>();
				ArrayList<BoolVar> casC_i_4 = new ArrayList<BoolVar>();
				ArrayList<BoolVar> casC_i_12 = new ArrayList<BoolVar>();
				ArrayList<BoolVar> casC_i_34 = new ArrayList<BoolVar>();
				ArrayList<BoolVar> casD_i = new ArrayList<BoolVar>();
				ArrayList<BoolVar> casD_i_1 = new ArrayList<BoolVar>();
				ArrayList<BoolVar> casD_i_2 = new ArrayList<BoolVar>();
				ArrayList<BoolVar> casD_i_3 = new ArrayList<BoolVar>();
				ArrayList<BoolVar> casD_i_4 = new ArrayList<BoolVar>();
				ArrayList<BoolVar> casD_i_12 = new ArrayList<BoolVar>();
				ArrayList<BoolVar> casD_i_34 = new ArrayList<BoolVar>();
				
				for(int j=0; j<c_m.get(i).size(); j++) {
					casA_i_1.add(model.arithm(zone1.get(i).get(j), "+",  sens1[i], "=", 2).reify());
					casA_i_2.add(model.arithm(zone4.get(i).get(j), "+",  sens2[i], "=", 2).reify());
					casA_i_3.add(model.arithm(zone3.get(i).get(j), "+",  sens3[i], "=", 2).reify());
					casA_i_4.add(model.arithm(zone2.get(i).get(j), "+",  sens4[i], "=", 2).reify());
					casA_i_12.add(model.arithm(casA_i_1.get(j), "+", casA_i_2.get(j), "=", 1).reify());
					casA_i_34.add(model.arithm(casA_i_3.get(j), "+", casA_i_4.get(j), "=", 1).reify());
					casA_i.add(model.arithm(casA_i_12.get(j), "+", casA_i_34.get(j), "=", 1).reify());
					casB_i_1.add(model.arithm(zone2.get(i).get(j), "+",  sens1[i], "=", 2).reify());
					casB_i_2.add(model.arithm(zone1.get(i).get(j), "+",  sens2[i], "=", 2).reify());
					casB_i_3.add(model.arithm(zone4.get(i).get(j), "+",  sens3[i], "=", 2).reify());
					casB_i_4.add(model.arithm(zone3.get(i).get(j), "+",  sens4[i], "=", 2).reify());
					casB_i_12.add(model.arithm(casB_i_1.get(j), "+", casB_i_2.get(j), "=", 1).reify());
					casB_i_34.add(model.arithm(casB_i_3.get(j), "+", casB_i_4.get(j), "=", 1).reify());
					casB_i.add(model.arithm(casB_i_12.get(j), "+", casB_i_34.get(j), "=", 1).reify());
					casC_i_1.add(model.arithm(zone3.get(i).get(j), "+",  sens1[i], "=", 2).reify());
					casC_i_2.add(model.arithm(zone2.get(i).get(j), "+",  sens2[i], "=", 2).reify());
					casC_i_3.add(model.arithm(zone1.get(i).get(j), "+",  sens3[i], "=", 2).reify());
					casC_i_4.add(model.arithm(zone4.get(i).get(j), "+",  sens4[i], "=", 2).reify());
					casC_i_12.add(model.arithm(casC_i_1.get(j), "+", casC_i_2.get(j), "=", 1).reify());
					casC_i_34.add(model.arithm(casC_i_3.get(j), "+", casC_i_4.get(j), "=", 1).reify());
					casC_i.add(model.arithm(casC_i_12.get(j), "+", casC_i_34.get(j), "=", 1).reify());
					casD_i_1.add(model.arithm(zone4.get(i).get(j), "+",  sens1[i], "=", 2).reify());
					casD_i_2.add(model.arithm(zone3.get(i).get(j), "+",  sens2[i], "=", 2).reify());
					casD_i_3.add(model.arithm(zone2.get(i).get(j), "+",  sens3[i], "=", 2).reify());
					casD_i_4.add(model.arithm(zone1.get(i).get(j), "+",  sens4[i], "=", 2).reify());
					casD_i_12.add(model.arithm(casD_i_1.get(j), "+", casD_i_2.get(j), "=", 1).reify());
					casD_i_34.add(model.arithm(casD_i_3.get(j), "+", casD_i_4.get(j), "=", 1).reify());
					casD_i.add(model.arithm(casD_i_12.get(j), "+", casD_i_34.get(j), "=", 1).reify());
				}
				casA.add(casA_i);
				casB.add(casB_i);
				casC.add(casC_i);
				casD.add(casD_i);
			}		
			
			/**
			 * Constraints
			 */
			
			//Ne pas sortir de la pi�ce 
			
			for (int i = 0; i < n_equipements; i++) {
				model.arithm(x_Var[i], "<=", L_local_Var, "-" , L_Var[i]).post(); 
				model.arithm(y_Var[i], "<=", l_local_Var, "-" , l_Var[i]).post(); 
				model.arithm(z_Var[i], "<=", h_local_Var, "-" , h_Var[i]).post(); 
				model.arithm(x_t_Var[i], "<=", L_local_Var, "-", L_t_Var[i]).post();
				model.arithm(y_t_Var[i], "<=", l_local_Var, "-", l_t_Var[i]).post();
				for(int j=0; j<casA.get(i).size(); j++) {
					model.ifThen(casB.get(i).get(j), model.arithm(x_m_Var.get(i).get(j), "<=", L_local_Var, "-", z_m_Var.get(i)));
					model.ifThen(casA.get(i).get(j), model.arithm(y_m_Var.get(i).get(j), "<=", l_local_Var, "-", z_m_Var.get(i)));
				}
			}
			
			//Impl�mentation des zones thermiques pour chaque �quipement
			
			for (int i = 0; i < n_equipements; i++) {
				model.arithm(x_t_Var[i], "=", x_Var[i], "-", z_t_Var[i]).post();
				model.arithm(y_t_Var[i], "=", y_Var[i], "-", z_t_Var[i]).post();
			}
			
			// Sens equipements
			
			for(int i=0; i<n_equipements; i++) {
				model.ifThen(c_horizontal[i], model.arithm(l_Var[i], "=", l.get(i)));
				model.ifThen(c_vertical[i], model.arithm(l_Var[i], "=", L.get(i)));
				model.ifThen(c_horizontal[i], model.arithm(L_Var[i], "=", L.get(i)));
				model.ifThen(c_vertical[i], model.arithm(L_Var[i], "=", l.get(i)));
				
				model.ifThen(c_horizontal[i], model.arithm(l_t_Var[i], "=", l_t[i]));
				model.ifThen(c_vertical[i], model.arithm(l_t_Var[i], "=", L_t[i]));
				model.ifThen(c_horizontal[i], model.arithm(L_t_Var[i], "=", L_t[i]));
				model.ifThen(c_vertical[i], model.arithm(L_t_Var[i], "=", l_t[i]));
			}
			
			// Sens zones de maintenance
			
			for(int i=0; i<n_equipements; i++) {
				for(int j=0; j<casA.get(i).size(); j++) {
					model.ifThen(casA.get(i).get(j), model.arithm(x_m_Var.get(i).get(j), "=", x_Var[i]));
					model.ifThen(casA.get(i).get(j), model.arithm(y_m_Var.get(i).get(j), "=", y_Var[i], "+", l_Var[i]));
					model.ifThen(casA.get(i).get(j), model.arithm(l_m_Var.get(i).get(j), "=", z_m_Var.get(i)));
					model.ifThen(casA.get(i).get(j), model.arithm(L_m_Var.get(i).get(j), "=", L_Var[i]));
				}
				for(int j=0; j<casB.get(i).size(); j++) {
					model.ifThen(casB.get(i).get(j), model.arithm(x_m_Var.get(i).get(j), "=", x_Var[i], "+", L_Var[i]));
					model.ifThen(casB.get(i).get(j), model.arithm(y_m_Var.get(i).get(j), "=", y_Var[i]));
					model.ifThen(casB.get(i).get(j), model.arithm(l_m_Var.get(i).get(j), "=", l_Var[i]));
					model.ifThen(casB.get(i).get(j), model.arithm(L_m_Var.get(i).get(j), "=", z_m_Var.get(i)));
				}
				for(int j=0; j<casC.get(i).size(); j++) {
					model.ifThen(casC.get(i).get(j), model.arithm(x_m_Var.get(i).get(j), "=", x_Var[i]));
					model.ifThen(casC.get(i).get(j), model.arithm(y_m_Var.get(i).get(j), "=", y_Var[i], "-", z_m_Var.get(i)));
					model.ifThen(casC.get(i).get(j), model.arithm(l_m_Var.get(i).get(j), "=", z_m_Var.get(i)));
					model.ifThen(casC.get(i).get(j), model.arithm(L_m_Var.get(i).get(j), "=", L_Var[i]));
				}
				for(int j=0; j<casD.get(i).size(); j++) {
					model.ifThen(casD.get(i).get(j), model.arithm(x_m_Var.get(i).get(j), "=", x_Var[i], "-", z_m_Var.get(i)));
					model.ifThen(casD.get(i).get(j), model.arithm(y_m_Var.get(i).get(j), "=", y_Var[i]));
					model.ifThen(casD.get(i).get(j), model.arithm(l_m_Var.get(i).get(j), "=", l_Var[i]));
					model.ifThen(casD.get(i).get(j), model.arithm(L_m_Var.get(i).get(j), "=", z_m_Var.get(i)));
				}
			}			

			model.diffN(x_Var, y_Var, L_Var, l_Var, false).post();
			model.diffN(x_t_Var, y_t_Var, L_t_Var, l_t_Var, false).post();
			
		


			
			for(int i=0; i<nb_equipements; i++) {
				for(int j=0; j<x_m_Var.get(i).size(); j++) {
					IntVar[] x_zoneMaintenanceEtEquipements = new IntVar[x_Var.length+1];
					IntVar[] y_zoneMaintenanceEtEquipements = new IntVar[y_Var.length+1];
					IntVar[] L_zoneMaintenanceEtEquipements = new IntVar[L_Var.length+1];
					IntVar[] l_zoneMaintenanceEtEquipements = new IntVar[l_Var.length+1];
					x_zoneMaintenanceEtEquipements[0] = x_m_Var.get(i).get(j);
					y_zoneMaintenanceEtEquipements[0] = y_m_Var.get(i).get(j);
					L_zoneMaintenanceEtEquipements[0] = L_m_Var.get(i).get(j);
					l_zoneMaintenanceEtEquipements[0] = l_m_Var.get(i).get(j);
					for(int k=0; k<x_Var.length; k++) {
						x_zoneMaintenanceEtEquipements[k+1] = x_Var[k];
						y_zoneMaintenanceEtEquipements[k+1] = y_Var[k];
						L_zoneMaintenanceEtEquipements[k+1] = L_Var[k];
						l_zoneMaintenanceEtEquipements[k+1] = l_Var[k];
					}
					model.diffN(x_zoneMaintenanceEtEquipements, y_zoneMaintenanceEtEquipements, L_zoneMaintenanceEtEquipements, l_zoneMaintenanceEtEquipements, false).post();
				}
				
			}
			
			/**
			 * Solution
			 */
			
			model.getSolver().showSolutions();
			
			List<Solution> ListSolutions = model.getSolver().findAllSolutions(new SolutionCounter(model, 1));
			model.getSolver().printStatistics();
			
			map.put("solCount", (int) model.getSolver().getSolutionCount());

			
			for(Solution s: ListSolutions ){ 
				int k=0;
				for (int i=0; i<nb_equipements; i++) {
					map(i+1, s.getIntVal(x_Var[i]), s.getIntVal(y_Var[i]), s.getIntVal(L_Var[i]), s.getIntVal(l_Var[i]));
					for(int j=0; j<casA.get(i).size(); j++) {
						map_z_m(k+1,s.getIntVal(x_m_Var.get(i).get(j)),s.getIntVal(y_m_Var.get(i).get(j)),s.getIntVal(L_m_Var.get(i).get(j)),s.getIntVal(l_m_Var.get(i).get(j)));
						k++;
					}
				}
				for (int i=0; i<nb_poteaux; i++) {
					map_poteau(i, s.getIntVal(x_Var[i+nb_equipements]), s.getIntVal(y_Var[i+nb_equipements]), s.getIntVal(L_Var[i+nb_equipements]), s.getIntVal(l_Var[i+nb_equipements]));
				}
			}
			
		}
	
		public void map(int i,int x, int y, int L, int l) {
			map.put("x_"+i, x); 
			map.put("y_"+i, y);
			map.put("L_"+i, L);
			map.put("l_"+i, l);
		}
		
		public void map_poteau(int i, int x, int y, int L, int l) {
			map.put("x_poteau_"+i, x);  
			map.put("y_poteau_"+i, y);
			map.put("L_poteau_"+i, L); 
			map.put("l_poteau_"+i, l);
		}
		
		public void map_z_m(int k, int x, int y, int L, int l) {
			map.put("x_m_"+k, x);
			map.put("y_m_"+k, y);
			map.put("L_m_"+k, L);
			map.put("l_m_"+k, l);
		}
		
		public void paintComponent(Graphics g){
	
			super.paintComponent(g);
			this.setBackground(Color.WHITE);
		
			ArrayList<Color> couleurs = new ArrayList<Color>();
			couleurs.add(Color.red);
			couleurs.add(Color.blue);
			couleurs.add(Color.green);
			couleurs.add(Color.orange);
			couleurs.add(Color.pink);
			couleurs.add(Color.gray);
			couleurs.add(Color.yellow);
			couleurs.add(Color.cyan);
			couleurs.add(Color.magenta);

			for (int k = 0; k<nb_zones_maintenance; k++){
				g.setColor(Color.gray);	
				g.drawRect(map.get("x_m_"+(k+1)),map.get("y_m_"+(k+1)),map.get("L_m_"+(k+1)),map.get("l_m_"+(k+1)));
			}			
					
			g.setColor(Color.black);
			g.drawRect(0, 0, map.get("L_local"), map.get("l_local"));
			
			g.setColor(Color.black);
			g.drawRect(map.get("x_porte"), map.get("y_porte"), map.get("l_porte"), map.get("l_porte"));
			g.fillRect(map.get("x_porte"), map.get("y_porte"), map.get("l_porte"), map.get("l_porte"));
			
			
			g.setColor(Color.black);
			for (int i = 0; i<nb_poteaux; i++){
				g.drawRect(map.get("x_poteau_"+(i)), map.get("y_poteau_"+(i)), map.get("L_poteau_"+(i)), map.get("l_poteau_"+(i)));
				g.fillRect(map.get("x_poteau_"+(i)), map.get("y_poteau_"+(i)), map.get("L_poteau_"+(i)), map.get("l_poteau_"+(i)));
			}

			for (int i = 0; i<nb_equipements; i++){
				g.setColor(couleurs.get(i));	
				g.drawRect(map.get("x_"+(i+1)),map.get("y_"+(i+1)),map.get("L_"+(i+1)),map.get("l_"+(i+1)));
			}	 
	
		}		
		
		public  void writeDataLineByLine(String filePath) { 

		    File file = new File(filePath); 
		    try { 
		    	
		    	// create FileWriter object with file as parameter 
		        FileWriter outputfile = new FileWriter(file); 
		  
		        // create CSVWriter object filewriter object as parameter 
		        CSVWriter writer = new CSVWriter(outputfile); 
		  
		        // adding header to csv 
		        String[] header = { "X0", "Y0" }; 
		        writer.writeNext(header); 
		  
		        // add data to csv 
		        String[] data1 = { ""+map.get("x_1"), ""+map.get("y_1")}; 
		        writer.writeNext(data1); 
		        String[] data2 = { ""+map.get("x_2"), ""+map.get("y_2")}; 
		        writer.writeNext(data2); 
		        String[] data3 = { ""+map.get("x_3"), ""+map.get("y_3")}; 
		        writer.writeNext(data3); 		
		        // closing writer connection 
		        writer.close(); 
		    } 
		    
		    catch (IOException e) { 
		        e.printStackTrace(); 
		    } 		
		}

}
