public class Porte {
	
	String nom;
	int x;
	int y;
	int largeur;
	int hauteur ;
	
	public String getNom() {
		return nom;
	}
	
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public int getX() {
		return x;
	}
	
	public void setX(int x) {
		this.x = x;
	}
	
	public int getY() {
		return y;
	}
	
	public void setY(int y) {
		this.y = y;
	}
	
	public int getLargeur() {
		return largeur;
	}
	
	public void setLargeur(int largeur) {
		this.largeur = largeur;
	}
	
	public int getHauteur() {
		return hauteur;
	}
	
	public void setHauteur(int hauteur) {
		this.hauteur = hauteur;
	}
	
	@Override
	public String toString() {
		return "Porte [nom=" + nom + ", x=" + x + ", y=" + y + ", largeur=" + largeur + ", hauteur=" + hauteur + "]";
	}
	
}
