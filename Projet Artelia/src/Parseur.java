import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.NPOIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.WorkbookUtil;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Parseur {
	
	private String nomfichier;
	private NPOIFSFileSystem fs;
	
	public Parseur(String nom) {
		this.nomfichier = nom;
	}
	
	public Porte getPorte() throws IOException {
		fs = new NPOIFSFileSystem(new File(this.nomfichier));
        HSSFWorkbook wb = new HSSFWorkbook(fs.getRoot(), true);
		Porte porte = new Porte();
        Sheet sheet = wb.getSheetAt(2);
        for (Row row : sheet) {
			if(row.getRowNum()==0){
        		continue;
        	}
			for(int i=0;i<row.getLastCellNum();i++) {
				String c = row.getCell(i).toString();
				switch (i) {
				case 1:
					porte.setNom(c);
					break;
				case 2:
					porte.setX((int)Double.parseDouble(c));
					break;
				case 3:
					porte.setY((int)Double.parseDouble(c));
					break;
				case 4:
					porte.setLargeur((int)Double.parseDouble(c));
					break;
				case 5:
					porte.setHauteur((int)Double.parseDouble(c));
					break;
				}
			}
        }
        fs.close();
		return porte;
	}
	
	public Local getLocal() throws IOException {
		fs = new NPOIFSFileSystem(new File(this.nomfichier));
        HSSFWorkbook wb = new HSSFWorkbook(fs.getRoot(), true);
        Local local = new Local();
        Sheet sheet = wb.getSheetAt(1);
        for (Row row : sheet) {
			if(row.getRowNum()==0){
        		continue;
        	}
			for(int i=0;i<row.getLastCellNum();i++) {
				String c = row.getCell(i).toString();
				switch (i) {
				case 1:
					local.setNom(c);
					break;
				case 2:
					local.setLongueur((int)Double.parseDouble(c));
					break;
				case 3:
					local.setLargeur((int)Double.parseDouble(c));
					break;
				case 4:
					local.setHauteur((int)Double.parseDouble(c));
					break;
				}
			}
		 }
			fs.close();
			return local;
		}

	public List<Equipement> getEquipements() throws IOException {
		fs = new NPOIFSFileSystem(new File(this.nomfichier));
        HSSFWorkbook wb = new HSSFWorkbook(fs.getRoot(), true);
        List<Equipement> equipements = new ArrayList<Equipement>();
        Sheet sheet = wb.getSheetAt(0);
        for (Row row : sheet) {
        	//remove header 
        	if(row.getRowNum()==0){
        		continue;
        	}
        	Equipement e = new Equipement();
        	for(int i=0;i<row.getLastCellNum();i++) {
        		String c = row.getCell(i).toString();
        		switch (i) {
				case 1:
					e.setNom(c);
					break;
				case 2:
					e.setLongueur((int)Double.parseDouble(c));
					break;
				case 3:
					e.setLargeur((int)Double.parseDouble(c));
					break;
					
				case 4:
					e.setHauteur((int)Double.parseDouble(c));
					break;
				case 5:
					e.setZone_thermique((int)Double.parseDouble(c));
					break;
				case 6:
					ArrayList<Integer> cote = new ArrayList<Integer>();
					if (c.contains("haut")) {
						cote.add(1);
					} 
					if (c.contains("droit") || c.contains("droite")) {
						cote.add(2);
					}
					if (c.contains("bas")) {
						cote.add(3);
					}
					if (c.contains("gauche")) {
						cote.add(4);
					}
					/*if (c.contains("non")) {
						cote.add(0);
					}*/
					e.setCote_maintenance(cote);
					break;
				case 7:
					e.setZone_maintenance((int)Double.parseDouble(c));
					break;
				case 8:
					if (c.equals("Oui")) {
						e.setMur(true);
					} else {
						e.setMur(false);
					}
					break;
				}
        	}
        	equipements.add(e);
        }
        
        fs.close();
        Equipement equipement = (Equipement) equipements.get(0);
        for (Object e : equipements) {
        	 System.out.println(e); 
        }
		return equipements;
	}
	
	public List<Poteau> getPoteaux() throws IOException {
		fs = new NPOIFSFileSystem(new File(this.nomfichier));
        HSSFWorkbook wb = new HSSFWorkbook(fs.getRoot(), true);
        List<Poteau> poteaux = new ArrayList<Poteau>();
        Sheet sheet = wb.getSheetAt(3);
        for (Row row : sheet) {
        	//remove header 
			if(row.getRowNum()==0) {
        		continue;
        	}
        	Poteau p = new Poteau();
			for(int i=0;i<row.getLastCellNum();i++) {
				String c = row.getCell(i).toString();
				switch (i) {
				case 1:
					p.setNom(c);
					break;
				case 2:
					p.setX((int)Double.parseDouble(c));
					break;
				case 3:
					p.setY((int)Double.parseDouble(c));
					break;
				case 4:
					p.setLongueur((int)Double.parseDouble(c));
					break;	
				case 5:
					p.setLargeur((int)Double.parseDouble(c));
					break;
				case 6:
					p.setHauteur((int)Double.parseDouble(c));
					break;
				}
			}
			poteaux.add(p);
        }
        fs.close();
        Poteau poteau = (Poteau) poteaux.get(0);
        for (Object p : poteaux) {
        	System.out.println(p);
        }
		return poteaux;
	}	
	
    public static void main(String[] args) throws Exception {
    	Parseur parseur = new Parseur("data.xls");
    	parseur.getEquipements();
    }
}

