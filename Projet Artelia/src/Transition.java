import java.awt.Color;
import java.awt.Graphics;
import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import org.chocosolver.solver.Model;
import org.chocosolver.solver.Solution;
import org.chocosolver.solver.constraints.Constraint;
import org.chocosolver.solver.search.limits.SolutionCounter;
import org.chocosolver.solver.variables.BoolVar;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.solver.variables.RealVar;
import org.chocosolver.solver.variables.SetVar;

import com.opencsv.CSVWriter;

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.*;

import javax.swing.JFrame;


public class Transition extends JPanel {
	
	/**
	 * Reference : K03-05
	 */

	public  Map<String,Integer> map = new HashMap<String,Integer>();
	
		public  void solveur() {

		/**
		 * Data
		 */
		
		//Local
		Equipement local = new Equipement(290, 395, 340);
			
		int l_local = local.getLargeur();
		int L_local = local.getLongueur();
		int h_local = local.getHauteur();
		
		map.put("L_local", L_local);
		map.put("l_local", l_local);
		map.put("h_local", h_local);
				
		
		// Porte
		Equipement porte = new Equipement(110, 210, 265, 0);
		
		int l_porte = porte.getLargeur();
		int h_Porte = porte.getHauteur();
		int x_porte = porte.getX();
		int y_porte = porte.getY();
		
		
		// �quipements
		int n_equipements = 3; // Nombre d'�quipements
	
		Equipement equip_1 = new Equipement(98, 170, 150, 40, 155); //transformateur
		Equipement equip_2 = new Equipement(94, 112, 160, 20, 150); //cellules de protection
		Equipement equip_3 = new Equipement(22, 100, 176, 0, 100); //panoplie de s�curit�
		
			/** 
			 * Transformateur
			 */
		
		int l_1 = equip_1.getLargeur(); 
		int L_1 = equip_1.getLongueur();
		int h_1 = equip_1.getHauteur();
		int z_t_1 = equip_1.getZone_thermique();
		int z_m_1 = equip_1.getZone_maintenance();
		
			/**
			 * Cellules de protection
			 */
		
		int l_2 = equip_2.getLargeur();
		int L_2 = equip_2.getLongueur();
		int h_2 = equip_2.getHauteur();
		int z_t_2 = equip_2.getZone_thermique();
		int z_m_2 = equip_2.getZone_maintenance();
		
			/**
			 * Panoplie s�curit�
			 */
		
		int l_3 = equip_3.getLargeur();
		int L_3 = equip_3.getLongueur();
		int h_3 = equip_3.getHauteur();
		int z_t_3 = equip_3.getZone_thermique();
		int z_m_3 = equip_3.getZone_maintenance();

			/**
			 * G�n�ralisation
			 */

		int[] l0 = {l_1, l_2, l_3};
		int[] L0 = {L_1, L_2, L_3};
		int[] z0_t = {z_t_1, z_t_2, z_t_3};
	
			/**
		 	* Zones thermiques
		 	*/
		int[] l0_t = new int[3];
		int[] L0_t = new int[3];
		for (int i = 0; i < n_equipements; i++) {
			l0_t[i] = l0[i] + 2*z0_t[i];
			L0_t[i] = L0[i] + 2*z0_t[i];
		}	
		
		/**
		 * Model
		 */
		Model model = new Model("Artelia");
		
		IntVar l0_local = model.intVar("l0_local", l_local); // largeur �quipement local
		IntVar L0_local = model.intVar("L0_local", L_local); // longueur �quipement local
		
		IntVar[] x = new IntVar[n_equipements + 1]; // liste des abscises des �quipements
		IntVar[] y = new IntVar[n_equipements + 1]; // liste des ordonn�es des �quipements
		IntVar[] l = new IntVar[n_equipements + 1]; // liste des largeurs des �quipements
		IntVar[] L = new IntVar[n_equipements + 1]; // liste des longueurs des �quipements
				
		IntVar[] z_t = new IntVar[n_equipements]; // liste des dimensions des zones thermiques
		IntVar[] x_t = new IntVar[n_equipements]; // liste des abscisses des zones thermiques
		IntVar[] y_t = new IntVar[n_equipements]; // liste des ordonn�es des zones thermiques
		IntVar[] l_t = new IntVar[n_equipements]; // liste des largeurs des zones thermiques
		IntVar[] L_t = new IntVar[n_equipements]; // liste des longueurs des zones thermiques
		
		IntVar[] sens = new IntVar[n_equipements];
		BoolVar[] sens1 = new BoolVar[n_equipements];
		BoolVar[] sens2 = new BoolVar[n_equipements];
		BoolVar[] sens3 = new BoolVar[n_equipements];
		BoolVar[] sens4 = new BoolVar[n_equipements];
		
		Constraint[] c_horizontal = new Constraint[n_equipements];
		Constraint[] c_vertical = new Constraint[n_equipements];

		/**
		 * Variables
		 */
		
			/**
			 * Variables �quipements 
			 */
		
		for (int i = 0; i < (n_equipements + 1); i++) {
			if (i < n_equipements) {
				x[i] = model.intVar("x0_"+i, 0, L_local, true); // position x 
				y[i] = model.intVar("y0_"+i, 0, l_local, true); // position y 
				l[i] = model.intVar("l0_"+i, new int[] {l_1, L_1}); // largeur
				L[i] = model.intVar("L0_"+i, new int[] {L_1, l_1}); // longueur
			} else {
				x[i] = model.intVar("x0_porte", x_porte);
				y[i] = model.intVar("y0_porte", y_porte);
				l[i] = model.intVar("l0_porte", l_porte);
				L[i] = model.intVar("L0_porte", l_porte);
			}
		}
		
			/**
			 * Sens
			 */
		
		for (int i = 0; i < (n_equipements); i++) {
			sens[i] = model.intVar("sens_"+i, 0, 3);
			sens1[i] = model.arithm(sens[i], "=", 0).reify();
			sens2[i] = model.arithm(sens[i], "=", 1).reify();
			sens3[i] = model.arithm(sens[i], "=", 2).reify();
			sens4[i] = model.arithm(sens[i], "=", 3).reify();
			c_horizontal[i] = model.arithm(sens1[i], "+", sens3[i], "=", 1);
			c_vertical[i] = model.arithm(sens2[i], "+", sens4[i], "=", 1);
			model.ifThen(c_horizontal[i], model.arithm(l[i], "=", l0[i]));
			model.ifThen(c_vertical[i], model.arithm(l[i], "=", L0[i]));
			model.ifThen(c_horizontal[i], model.arithm(L[i], "=", L0[i]));
			model.ifThen(c_vertical[i], model.arithm(L[i], "=", l0[i]));
		}
		
			/**
			 * Variables zones thermiques
			 */
		
		for (int i = 0; i < n_equipements; i++) {
			z_t[i] = model.intVar("z0_t_"+i, z0_t[i]);
			x_t[i] = model.intVar("x0_t_"+i, 0, L_local, true); 
			y_t[i] = model.intVar("y0_t_"+i, 0, l_local, true); 
			l_t[i] = model.intVar("l0_t_"+i, l0_t[i]);
			L_t[i] = model.intVar("L0_t_"+i, L0_t[i]);
		}
		
		/**
		 * Constraints
		 */
		
			/**
			 * Ne pas sortir de la pi�ce 
			 */
		
			for (int i = 0; i < n_equipements; i++) {
				model.arithm(x[i], "<=", L0_local, "-" , L[i]).post(); 
				model.arithm(y[i], "<=", l0_local, "-" , l[i]).post(); 
				model.arithm(x_t[i], "<=", L0_local, "-", L[i]).post();
				model.arithm(y_t[i], "<=", l0_local, "-", l[i]).post();
			}
			
			/**
			 * Impl�mentation des zones thermiques pour chaque �quipement
			 */
			
			for (int i = 0; i < n_equipements; i++) {
				model.arithm(x_t[i], "=", x[i], "-", z_t[i]).post();
				model.arithm(y_t[i], "=", y[i], "-", z_t[i]).post();
			}
			
		model.diffN(x, y, L, l, false).post(); // Non superposition des �quipements
		model.diffN(x_t, y_t, L_t, l_t, false).post();	// Non superposition des zones thermiques

		/**s
		 * Solution
		 */

		model.getSolver().showSolutions();

		List<Solution> ListSolutions = model.getSolver().findAllSolutions(new SolutionCounter(model, 1));
		model.getSolver().printStatistics();
		
		for(Solution s: ListSolutions ){ 
				map(1, s.getIntVal(x[0]), s.getIntVal(y[0]), s.getIntVal(L[0]), s.getIntVal(l[0]));
				map(2, s.getIntVal(x[1]), s.getIntVal(y[1]), s.getIntVal(L[1]), s.getIntVal(l[1]));
				map(3, s.getIntVal(x[2]), s.getIntVal(y[2]), s.getIntVal(L[2]), s.getIntVal(l[2]));
			}
		}

	
	public void map(int i,int x, int y, int L, int l) {
		map.put("x_"+i, x);
		map.put("y_"+i, y);
		map.put("L_"+i, L);
		map.put("l_"+i, l);
	}
	
	public void paintComponent(Graphics g ){
		super.paintComponent(g);
		this.setBackground(Color.WHITE);
		
		g.setColor(Color.black);
		g.drawRect(0, 0, map.get("L0_local"), map.get("l0_local"));
		//drawRect(0,0,L_local,l_local)

		//for (int i = 1;  i<=n_equipements; i++){
		g.setColor(Color.red);
		g.drawRect(map.get("x0_1"),map.get("y0_1"),map.get("L0_1"),map.get("l0_1")); // � remplacer avec dimensions equipements
		//drawRect(x,y,largeur,hauteur)
		
		g.setColor(Color.blue);
		g.drawRect(map.get("x0_2"),map.get("y0_2"),map.get("L0_2"),map.get("l0_2")); // � remplacer avec dimensions equipements
		//drawRect(x,y,largeur,hauteur)
		
		g.setColor(Color.green);
		g.drawRect(map.get("x0_3"),map.get("y0_3"),map.get("L0_3"),map.get("l0_3")); // � remplacer avec dimensions equipements
		//drawRect(x,y,largeur,hauteur)	
		
		}
		
	public  void writeDataLineByLine(String filePath) 
	{ 
	    File file = new File(filePath); 
	    try { 
	    	
	    	
	        // create FileWriter object with file as parameter 
	        FileWriter outputfile = new FileWriter(file); 
	  
	        // create CSVWriter object filewriter object as parameter 
	        CSVWriter writer = new CSVWriter(outputfile); 
	  
	        // adding header to csv 
	        String[] header = { "X0", "Y0" }; 
	        writer.writeNext(header); 
	  
	        // add data to csv 
	        String[] data1 = { ""+map.get("x0_1"), ""+map.get("y0_1")}; 
	        writer.writeNext(data1); 
	        String[] data2 = { ""+map.get("x0_2"), ""+map.get("y0_2")}; 
	        writer.writeNext(data2); 
	        String[] data3 = { ""+map.get("x0_3"), ""+map.get("y0_3")}; 
	        writer.writeNext(data3); 
	  
	  
	        // closing writer connection 
	        writer.close(); 
	    } 
	    catch (IOException e) { 
	        e.printStackTrace(); 
	    } 
	    
	    
	} 
	
	

}

// 	x0_1 = 40 y0_1 = 40 x0_2 = 270 y0_2 = 110 x0_3 = 0 y0_3 = 178 
//  x0_0 = 40 y0_0 = 40 x0_1 = 270 y0_1 = 20 x0_2 = 250 y0_2 = 134 